import React from 'react'

import styles from './square.module.css'

function Square(props) {
    const {value, handleClick, index} = props

    const handleSquareClick = () => {
        handleClick(index);
    }

    return (
        <div className={styles.square} onClick={handleSquareClick}>
            {value}
        </div>
    )
}

export default Square
