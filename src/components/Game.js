import React, { useState, useEffect } from "react";

import Board from "./Board";

import { calculateWinner, isDraw } from "../services/gameLogic";

function Game() {
    const [board_values, setBoard_values] = useState(Array(9).fill(null));

    const [is_game_over, setIs_game_over] = useState(false);

    const [currentPlayer, setCurrentPlayer] = useState("X");

    const [winner, setWinner] = useState("");

    useEffect(() => {
        console.log("checking for draw");
        console.log(board_values);
        console.log(isDraw(board_values));

        if(winner != ''){
            setIs_game_over(true)
            restart()
        }
        
    }, [board_values]);

    const handleClick = (index) => {
        if (board_values[index] || is_game_over) {
            return;
        }

        const new_board_values = [...board_values];
        new_board_values[index] = currentPlayer;
        setBoard_values(new_board_values);

        if (isDraw(new_board_values)) {
            setWinner("Draw!");
        }

        if (calculateWinner(new_board_values)) {
            setWinner(currentPlayer);
        }
        currentPlayer === "X" ? setCurrentPlayer("O") : setCurrentPlayer("X");
    };

    const restart = () => {
        let should_restart = window.confirm("Restart?")
        if(should_restart === false){
            return
        }
        setIs_game_over(false);
        setWinner("");
        setCurrentPlayer("X");
        const new_board_values = Array(9).fill(null);
        setBoard_values(new_board_values);
    };

    return (
        <div>
            <h2>Tic-Tac-Toe</h2>
            <Board board_values={board_values} handleClick={handleClick} />
            {currentPlayer != "" && <div>{`currentPlayer: ${currentPlayer}`}</div>}
            {winner != "" && <div>{`winner: ${winner}`}</div>}
            {is_game_over && <div>Game Over!</div>}
            <button onClick={restart}>Restart</button>
        </div>
    );
}

export default Game;
