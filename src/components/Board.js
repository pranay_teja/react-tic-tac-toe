import React from "react";
import Square from "./Square";

import styles from "./board.module.css";

function Board(props) {
    const { board_values, handleClick } = props;

    const squares = board_values.map((value, index) => (
        <Square key={index} value={value} handleClick={handleClick} index={index}/>
    ));

    return <div className={styles.board}>{squares}</div>;
}

export default Board;
