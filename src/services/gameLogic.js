export const calculateWinner = (board_values) => {
    const winning_cases = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];

    for (let i = 0; i < winning_cases.length; i++) {
        const [a, b, c] = winning_cases[i];
        if (
            board_values[a] &&
            board_values[a] === board_values[b] &&
            board_values[a] === board_values[c]
        ) {
            return board_values[a];
        }
    }

    return null;
}

export const isDraw = (board_values) => {
    for (let i = 0; i < board_values.length; i++) {
        if (board_values[i] == null) {
            return false;
        }
    }

    return true;
}

// export default {isDraw, calculateWinner};

